package BoolFunction;

import BoolFunction.Nodes.Equal;
import BoolFunction.Nodes.Node;
import BoolFunction.Nodes.Variable;
import Main.Main;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FunctionTools {

    private FunctionTools() {
    }

    public static Specials findSpecials(Node node) {
        boolean[][] arr = FunctionTools.evaluate(node);

        boolean allTrue = true, allFalse = true;
        for (boolean[] a : arr) {
            if (a[a.length - 1]) {
                allFalse = false;
            }
            else {
                allTrue = false;
            }
        }

        if (allTrue) {
            return Specials.ALL_TRUE;
        }
        if (allFalse) {
            return Specials.ALL_FALSE;
        }

        return Specials.NONE;
    }

    public static void table(Node... nodes) {
        Objects.requireNonNull(nodes);
        if (nodes.length == 0) {
            throw new IllegalArgumentException("Need at least one node");
        }
        
        System.out.println("Table");
        System.out.println("=====");
        for (int x = 0; x < nodes.length; x++) {
            System.out.printf("\"%c = %s\"\n", (char) ('f' + x), nodes[x].toString());
        }
        System.out.println("");
        
        tableFor(nodes);
    }

    public static void testEqual(Node a, Node b) {
        System.out.printf("Testing \"f = %s <=> %s\"\n\n", a.toString(), b.toString());
        Node equal = new Equal(a, b);
        boolean[][] arr = evaluate(equal);

        int failed = 0;
        for (boolean[] sub : arr) {
            if (!sub[sub.length - 1]) {
                failed++;
            }
        }

        if (failed > 0) {
            System.out.printf("\n[FAILED] Functions differ in %d cases\n", failed);
        }
        else {
            System.out.printf("\n[OK] Functions are equal\n", failed);
        }
    }

    public static Node simplify(Node f) {
        return simplify(f, false);
    }

    public static Node simplify(Node f, boolean log) {
        if (log) {
            System.out.println("Simplifying \"" + f.toString() + "\"");
        }
        final int fHash = f.hashCode();
        int hash = fHash, last = 0;
        Node n = f.simplify();

        while (hash != last) {
            last = hash;

            n = n.simplify();
            hash = n.hashCode();
            if (log && hash != last) {
                if (Main.VERBOSE) {
                    System.out.println("");
                }
                System.out.println("<=> " + n.toString());
            }

            if (fHash != f.hashCode()) {
                throw new IllegalStateException("F has changed (Last simpflification by "
                        + n.getClass().getName() + ")");
            }

            if (findSpecials(new Equal(f, n)) != Specials.ALL_TRUE) {
                throw new IllegalStateException("Bad simplification (Last simpflification by "
                        + n.getClass().getName() + ")");
            }
        }

        return n;
    }

    public static boolean[][] evaluate(Node node) {
        return evaluate(node, node.getVariables());
    }

    private static void tableFor(Node... nodes) {
        Objects.requireNonNull(nodes);
        if (nodes.length == 0) {
            throw new IllegalArgumentException("Need at least one node");
        }

        List<Variable> vars = new ArrayList<>(5);
        for (Node node : nodes) {
            for (Variable v : node.getVariables()) {
                if (!vars.contains(v)) {
                    vars.add(v);
                }
            }
        }
        vars.sort((Variable a, Variable b) -> {
            return Integer.compare(b.getID(), a.getID());
        });

        for (Variable var : vars) {
            System.out.print(var.toString() + " | ");
        }

        for (int x = 0; x < nodes.length; x++) {
            System.out.print((char) ('f' + x) + (x < nodes.length - 1 ? "  | " : "\n"));
        }

        for (int x = 0; x < vars.size() + nodes.length + 1; x++) {
            System.out.print("----");
        }
        System.out.println("-");

        ArrayList<boolean[][]> results = new ArrayList<>(nodes.length);
        for (Node node : nodes) {
            results.add(evaluate(node, vars));
        }

        for (int x = 0; x < results.get(0).length; x++) {
            for (int y = 0; y < results.get(0)[x].length - 1; y++) {
                System.out.print((results.get(0)[x][y] ? "1" : "0"));
                System.out.print("  | ");
            }

            for (int y = 0; y < nodes.length; y++) {
                System.out.print((results.get(y)[x][results.get(y)[x].length - 1] ? "1" : "0"));
                if (y < nodes.length - 1) {
                    System.out.print("  | ");
                }
            }

            System.out.println("");
        }
    }

    private static boolean[][] evaluate(Node node, List<Variable> vars) {
        boolean[][] arr = new boolean[(int) Math.pow(2, vars.size())][vars.size() + 1];
        evalPart(node, vars, 0, new int[]{0}, arr);
        return arr;
    }

    private static void evalPart(Node node, List<Variable> vars, int n, int[] i, boolean[][] arr) {
        if (n < vars.size()) {
            vars.get(n).setValue(false);
            evalPart(node, vars, n + 1, i, arr);
            vars.get(n).setValue(true);
            evalPart(node, vars, n + 1, i, arr);
        }
        else {
            for (int x = 0; x < vars.size(); x++) {
                arr[i[0]][x] = vars.get(x).getValue();
            }
            arr[i[0]][arr[i[0]].length - 1] = node.evaluate();
            i[0]++;
        }
    }

    public enum Specials {
        ALL_TRUE, ALL_FALSE, NONE
    }

}
