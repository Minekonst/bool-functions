package BoolFunction.Nodes;

import BoolFunction.Simplification.MultiOperantSimplifications;
import Main.Main;
import java.util.function.Function;
import java.util.ArrayList;

public class And extends MultiOperantNode {

    /**
     * Create a new And-Node
     *
     * @param children The children (operants)
     *
     * @throws IllegalArgumentException if less than 2 children are passed
     */
    public And(Node... children) {
        super(children);
    }

    @Override
    public boolean evaluate() {
        boolean v = true;
        for (Node n : children) {
            v &= n.evaluate();
        }

        return v;
    }

    @Override
    public Node simplify() {
        Node s = super.simplify();
        if (s != this) {
            return s;
        }

        for (Function<MultiOperantNode, Node[]> f : SIMPLIFICATIONS) {
            Node[] n = f.apply(this);
            if (n != null) {
                return new And(n);
            }
        }

        ArrayList<Node> sn = new ArrayList<>(children.length);
        boolean childrenSimple = false;

        for (Node child : children) {
            if (child instanceof Static) {
                if (child.evaluate()) {
                    if (Main.VERBOSE) {
                        System.out.println("└ Subterm of And \"" + child.toString() + "\" is redundant");
                    }

                    childrenSimple = true;
                    continue;
                }
                else {
                    if (Main.VERBOSE) {
                        System.out.println("└ Subterm of And \"" + child.toString() + "\" makes this expression always 0");
                    }
                    return new Static(false);
                }
            }

            Node si = childrenSimple ? child : child.simplify();
            childrenSimple |= (si != child);
            sn.add(si);
        }

        if (childrenSimple) {
            if (sn.isEmpty()) {
                return new Static(true);
            }
            else if (sn.size() == 1) {
                return sn.get(0).simplify();
            }
            return new And(sn.toArray(new Node[sn.size()]));
        }
        else {
            if (children.length == 2) {
                Node a, b;
                if (children[0] instanceof Or) {
                    a = children[1];
                    b = children[0];
                }
                else {
                    a = children[0];
                    b = children[1];
                }

                if (b instanceof Or) {
                    Or or = (Or) b;
                    Node[] nn = new Node[or.getChildren().length];
                    for (int x = 0; x < nn.length; x++) {
                        nn[x] = new And(a, or.getChildren()[x]);
                    }

                    Node n = new Or(nn);

                    if (Main.VERBOSE) {
                        System.out.println("└ Multiplying \"" + this.toString() + "\" out");
                    }

                    return n;
                }
            }

            return this;
        }
    }

    @Override
    public String toString() {
        String s = "(";
        for (int x = 0; x < children.length; x++) {
            s += (x > 0 ? " * " : "") + children[x].toString();
        }

        return s + ")";
    }
}
