package BoolFunction.Nodes;

public class Implication extends Node {

    private final Node a;
    private final Node b;

    public Implication(Node a, Node b) {
        super(new Node[]{a, b});
        
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean evaluate() {        
        return a.evaluate() || !b.evaluate();
    }
    
    @Override
    public String toString() {
        return a.toString() + " => " + b.toString();
    }
}
