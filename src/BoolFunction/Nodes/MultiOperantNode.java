package BoolFunction.Nodes;

import BoolFunction.Simplification.MultiOperantSimplifications;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public abstract class MultiOperantNode extends Node {

    public static final List<Function<MultiOperantNode, Node[]>> SIMPLIFICATIONS = Arrays.asList(
            MultiOperantSimplifications::assoziative
    );

    protected final Node[] children;

    protected MultiOperantNode(Node... children) {
        super(children);

        if (children.length < 2) {
            throw new IllegalArgumentException("Need at least two parents");
        }

        this.children = children;
    }

    public Node[] getChildren() {
        return children;
    }
}
