package BoolFunction.Nodes;

import BoolFunction.FunctionTools;
import BoolFunction.FunctionTools.Specials;
import Main.Main;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public abstract class Node {

    protected final ArrayList<Variable> vars = new ArrayList<>(5);

    Node(Node... children) {
        Objects.requireNonNull(children);

        for (Node n : children) {
            for (Variable v : n.getVariables()) {
                if (!vars.contains(v)) {
                    vars.add(v);
                }
            }
        }
    }

    public abstract boolean evaluate();

    @Override
    public abstract String toString();

    public List<Variable> getVariables() {
        vars.sort((a, b) -> {
            return Integer.compare(b.getID(), a.getID());
        });
        return Collections.unmodifiableList(vars);
    }

    public Node simplify() {
        if (!(this instanceof Static || this instanceof Variable)) {
            Specials s = FunctionTools.findSpecials(this);

            if (s == Specials.ALL_TRUE) {
                if (Main.VERBOSE) {
                    System.out.println("└ \"" + this.toString() + "\" is always 1");
                }
                
                return new Static(true);
            }
            else if (s == Specials.ALL_FALSE) {
                if (Main.VERBOSE) {
                    System.out.println("└ \"" + this.toString() + "\" is always 0");
                }
                
                
                return new Static(false);
            }
        }

        return this;
    }
}
