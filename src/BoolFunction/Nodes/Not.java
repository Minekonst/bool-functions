package BoolFunction.Nodes;

import Main.Main;

public class Not extends Node {

    private final Node child;

    public Not(Node child) {
        super(child);

        this.child = child;
    }

    @Override
    public boolean evaluate() {
        return !child.evaluate();
    }

    @Override
    public String toString() {
        return "|" + child.toString() + "|";
    }

    @Override
    public Node simplify() {
        Node s = super.simplify();
        if (s != this) {
            return s;
        }

        if (child instanceof Not) {
            if (Main.VERBOSE) {
                System.out.println("└ Double not: \"" + this.toString() + "\"");
            }
            return ((Not) child).getChild();
        }
        else if (child instanceof MultiOperantNode) {
            Node[] ori = ((MultiOperantNode) child).getChildren();
            Node[] nodes = new Node[ori.length];
            for (int x = 0; x < nodes.length; x++) {
                nodes[x] = new Not(ori[x]);
            }

            Node n;
            if (child instanceof And) {
                n = new Or(nodes);
            }
            else if (child instanceof Or) {
                n = new And(nodes);
            }
            else {
                throw new UnsupportedOperationException("Unsupported Type of MultiOperantNode for De Morgan");
            }

            if (Main.VERBOSE) {
                System.out.println("└ De Morgan \"" + this.toString() + "\" <=> \"" + n.toString() + "\"");
            }

            return n;
        }
        else if (child instanceof Static) {
            return new Static(!child.evaluate());
        }
        else {
            Node ps = child.simplify();
            if (child != ps) {
                return new Not(ps);
            }
            else {
                return this;
            }
        }
    }

    Node getChild() {
        return child;
    }
}
