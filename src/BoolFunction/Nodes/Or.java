package BoolFunction.Nodes;

import BoolFunction.FunctionTools;
import BoolFunction.FunctionTools.Specials;
import BoolFunction.Simplification.MultiOperantSimplifications;
import Main.Main;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Or extends MultiOperantNode {

    public Or(Node... children) {
        super(children);
    }

    @Override
    public boolean evaluate() {
        boolean v = false;
        for (Node n : children) {
            v |= n.evaluate();
        }

        return v;
    }

    @Override
    public Node simplify() {
        Node s = super.simplify();
        if (s != this) {
            return s;
        }

        for (Function<MultiOperantNode, Node[]> f : SIMPLIFICATIONS) {
            Node[] n = f.apply(this);
            if (n != null) {
                return new Or(n);
            }
        }

        ArrayList<Node> sn = new ArrayList<>(children.length);
        boolean childrenSimple = false;
        boolean allAnd = true;
        boolean oneAndTest = false;

        for (Node child : children) {
            if (child instanceof Static) {
                if (!child.evaluate()) {
                    if (Main.VERBOSE) {
                        System.out.println("└ Subterm of Or \"" + child.toString() + "\" is redundant");
                    }
                    childrenSimple = true;
                    continue;
                }
                else {
                    if (Main.VERBOSE) {
                        System.out.println("└ Subterm of Or \"" + child.toString() + "\" makes this expression always 1");
                    }
                    return new Static(true);
                }
            }

            Node si = childrenSimple ? child : child.simplify();
            childrenSimple |= si != child;
            sn.add(si);

            if (!(child instanceof Variable || child instanceof Not)) {
                allAnd &= child instanceof And;
                oneAndTest = true;
            }
        }

        if (childrenSimple) {
            if (sn.isEmpty()) {
                if (Main.VERBOSE) {
                    System.out.println("└ Empty Or");
                }
                return new Static(false);
            }
            else if (sn.size() == 1) {
                if (Main.VERBOSE) {
                    System.out.println("└ Or with one operant \"" + sn.get(0).toString() + "\" is redundant");
                }
                return sn.get(0).simplify();
            }
            return new Or(sn.toArray(new Node[sn.size()]));
        }
        else if (allAnd && oneAndTest) {
            List<Node> c = new ArrayList<>(Arrays.asList(children));

            outer:
            for (Node a : c) {
                for (Node b : c) {
                    if (a == b) {
                        continue;
                    }

                    Node or = new Or(a, b);
                    Specials sp = FunctionTools.findSpecials(new Implication(a, or));
                    if (sp == Specials.ALL_TRUE) {
                        c.remove(b);

                        if (Main.VERBOSE) {
                            System.out.printf("└ Or subterm \"%s\" is implied by \"%s\"\n",
                                    or.toString(), a.toString());
                        }

                        break outer;
                    }
                }
            }

            if (c.size() != children.length) {
                if (c.size() > 1) {
                    Node n = new Or(c.toArray(new Node[c.size()]));
                    return n.simplify();
                }
                else {
                    if (Main.VERBOSE) {
                        System.out.println("└ Or with one operant \"" + sn.get(0).toString() + "\" is redundant");
                    }
                    return c.get(0);
                }
            }
        }

        return this;
    }

    @Override
    public String toString() {
        String s = "(";
        for (int x = 0; x < children.length; x++) {
            s += (x > 0 ? " + " : "") + children[x].toString();
        }

        return s + ")";
    }
}
