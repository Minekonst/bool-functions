package BoolFunction.Nodes;

public class Static extends Node {

    private final boolean value;

    public Static(boolean value) {
        this.value = value;
    }

    @Override
    public boolean evaluate() {
        return value;
    }

    @Override
    public String toString() {
        return value ? "1" : "0";
    }

}
