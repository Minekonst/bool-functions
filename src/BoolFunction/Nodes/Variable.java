package BoolFunction.Nodes;

public class Variable extends Node {

    private final String name;
    private boolean value;
    private final int id;

    public Variable(int id) {
        if (id < 0) {
            throw new IllegalArgumentException("Id must be >= 0");
        }

        this.name = "x" + id;
        this.id = id;
        super.vars.add(this);
    }

    @Override
    public boolean evaluate() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
    
    public int getID() {
        return id;
    }

}
