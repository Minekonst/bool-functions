package BoolFunction.Simplification;

import BoolFunction.FunctionTools;
import BoolFunction.Nodes.And;
import BoolFunction.Nodes.Equal;
import BoolFunction.Nodes.MultiOperantNode;
import BoolFunction.Nodes.Node;
import BoolFunction.Nodes.Not;
import BoolFunction.Nodes.Or;
import BoolFunction.Nodes.Variable;
import Main.Main;
import java.util.ArrayList;
import java.util.Arrays;

public class MultiOperantSimplifications {

    private MultiOperantSimplifications() {

    }

    public static Node[] equalSubterms(MultiOperantNode node) {
        ArrayList<Node> children = new ArrayList<>(Arrays.asList(node.getChildren()));

        for (Node child : children) {
            for (Node child2 : children) {
                if (child2 == child) {
                    continue;
                }

                if (FunctionTools.findSpecials(new Equal(child, child2)) == FunctionTools.Specials.ALL_TRUE) {
                    if (Main.VERBOSE) {
                        System.out.printf("└ Subterms of %s \"%s\" and \"%s\" are equal", 
                                node.getClass().getSimpleName(), child.toString(), child2.toString());
                    }
                    
                    children.remove(child2);
                    return children.toArray(new Node[children.size()]);
                }
            }
        }
        
        return null;
    }
    
    public static Node[] assoziative(MultiOperantNode node) {
        boolean allSame = true;
        boolean one = false;
        
        for (Node child : node.getChildren()) {
            if (!(child instanceof Variable || child instanceof Not) &&
                    (node.getClass().equals(Or.class) ? !(child instanceof And) : true)) {
                allSame &= node.getClass().isInstance(child);
                one = true;
            }
        }
        
        if (allSame && one) {
            ArrayList<Node> c = new ArrayList<>(node.getChildren().length);
            for (Node child : node.getChildren()) {
                if (node.getClass().isInstance(child)) {
                    for (Node sub : ((MultiOperantNode) child).getChildren()) {
                        c.add(sub);
                    }
                }
                else {
                    c.add(child);
                }
            }

            if (Main.VERBOSE) {
                System.out.println("└ Assoziative " + node.getClass().getSimpleName());
            }

            return c.toArray(new Node[c.size()]);
        }
        
        return null;
    }
}
