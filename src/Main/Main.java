package Main;

import UI.MainFrame;

public class Main {

    public static final boolean VERBOSE = true;

    public static void main(String[] args) {
		/*
		 * Variable x0 = new Variable(0); Variable x1 = new Variable(1); Variable x2 =
		 * new Variable(2); Variable x3 = new Variable(3);
		 * 
		 * Node l14 = new Not(x0); Node l13 = new Not(new And(x1, new And(x1, x0)));
		 * Node l21 = new Or(l13, l14);
		 * 
		 * 
		 * Node l9 = new Or(x2, x1); Node l8 = new And(x3, x1); Node l15 = new Not(l8);
		 * Node l16 = new Or(l8, l9); Node l18 = new And(l15, l16); Node l20 = new
		 * Or(l18, l16); Node l22 = new And(l13, l20);
		 * 
		 * Node a = new Or(l21, l22);
		 */
    	
    	new MainFrame();
    }

}
