package Main;

@FunctionalInterface
public interface SimpleBoolFunction {

    static final boolean[] POSIBILITIES = {false, true};

    public boolean apply(boolean x3, boolean x2, boolean x1, boolean x0);

    public static void testFunction(SimpleBoolFunction f1, SimpleBoolFunction f2) {
        for (boolean x3 : POSIBILITIES) {
            for (boolean x2 : POSIBILITIES) {
                for (boolean x1 : POSIBILITIES) {
                    for (boolean x0 : POSIBILITIES) {
                        boolean r1 = f1.apply(x3, x2, x1, x0);
                        boolean r2 = f2.apply(x3, x2, x1, x0);

                        if (r1 == r2) {
                            print(x3, x2, x1, x0, r1, r2, "OK");
                        }
                        else {
                            print(x3, x2, x1, x0, r1, r2, "FAILED");
                        }
                    }
                }
            }
        }
    }

    private static void print(boolean x3, boolean x2, boolean x1, boolean x0,
            boolean r1, boolean r2, String status) {
        System.out.printf("%d | %d | %d | %d >> %d <-> %d [%s]\n", x3 ? 1 : 0, x2 ? 1 : 0,
                x1 ? 1 : 0, x0 ? 1 : 0, r1 ? 1 : 0, r2 ? 1 : 0, status);
    }
}
