package UI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import BoolFunction.Nodes.*;

public class InputParser {
	private InputParser() {}
	
	/*
	 * Parses an input given by the user to a
	 * Bool-Node
	 */
	public static Node parseInput(String input) {
		String[] lines = input.split("\n");
		
		// HashMap will be used to quickly access variables (x1, x2, ...)
		HashMap<Integer, Variable> variables = new HashMap<Integer, Variable>();
		// likewise for Nodes
		HashMap<Integer, Node> nodes = new HashMap<Integer, Node>();
		
		
		// Each line is an expression itself
		for (String line : lines) {
			// skip empty lines
			if (line.isEmpty()) continue;
			// skip lines beginning with #
			if (line.startsWith("#")) continue;
			
			// A line can be an assertion
			if (line.contains("=")) {
				// We split between <nodeName> = <expression>
				String nodeName = line.split("=")[0].strip();
				String expression = line.split("=")[1].strip();
				
				
				Node n = parseExpression(expression, variables, nodes);
				// a node will be named n<number>
				if (!nodeName.startsWith("n")) {
					throw new IllegalArgumentException("Node name does not start with n: " + nodeName);
				}
				
				int nodeNumber;
				try {
					nodeNumber = Integer.parseInt(nodeName.substring(1));
				} catch (NumberFormatException e) {
					throw new IllegalArgumentException("Node name is not witten like n<number> : " + nodeName);
				}

				// Check if the node already exists 
				if (nodes.containsKey(nodeNumber)) {
					throw new IllegalArgumentException("Node " + nodeName + " already defined.");
				} else {
					nodes.put(nodeNumber, n);
				}
			// One line can return a node which will be simplified
			} else if (line.startsWith("-> ")) {
				Node n = parseExpression(line.substring(3), variables, nodes);
				return n;
			} else {
				throw new IllegalArgumentException("Could not parse line " + line);
			}
		}
		throw new IllegalArgumentException("Input does not contain '-> ' for selecting a node");
	}

	private static Node parseExpression(String expression, HashMap<Integer, Variable> variables, HashMap<Integer, Node> nodes) {
		expression = expression.toLowerCase().strip();
		
		// Case 1: The expression contains OR, NOT or AND
		Pattern p = Pattern.compile("^(or|not|and)\\((.*)\\)$");
		Matcher m = p.matcher(expression);
		
		if (m.matches()) {
			// Get the operation (OR,AND or NOT)
			String operation = m.group(1).toLowerCase().strip();
			// get the arguments
			String[] args = parseArgs(m.group(2));
			
			// each argument is an expression itself that will be parsed
			// e.g. AND(NOT(x1), x2), arguments: ['NOT(x1)', 'x2']
			Node[] nodeArgs = new Node[args.length];
			for (int i = 0; i < args.length; i++) {
				nodeArgs[i] = parseExpression(args[i], variables, nodes);
			}
			
			// Returning the specified Node depending on the operation
			if (operation.equals("or")) {
				return new Or(nodeArgs);
			} else if (operation.equals("and")) {
				return new And(nodeArgs);
			} else if (operation.equals("not")) {
				if (nodeArgs.length != 1) {
					throw new IllegalArgumentException("Operation 'not' called with more than 1 argument : " + expression);
				}
				return new Not(nodeArgs[0]);
			} else {
				throw new IllegalArgumentException("Operation " + operation + " unknown");
			}
		// An expression can be a variable
		} else if (expression.startsWith("x")) {
			int variableNumber = Integer.parseInt(expression.substring(1));
			
			if (variables.containsKey(variableNumber)) {
				return variables.get(variableNumber);
			} else {
				// if the variable does not exist already, create it.
				Variable v = new Variable(variableNumber);
				variables.put(variableNumber, v);
				return v;
			}
		// An expression can be another node 
		} else if (expression.startsWith("n")) {
			int nodeNumber = Integer.parseInt(expression.substring(1));
			// A node must exists before using it inside an expression
			if (nodes.containsKey(nodeNumber)) {
				return nodes.get(nodeNumber);
			} else {
				throw new IllegalArgumentException("Using node " + nodeNumber + " before declaration.");
			}
		// Static values: 1 (TRUE), 0 (FALSE)
		} else if (expression.strip().equals("1")) {
			return new Static(true);
		} else if (expression.strip().equals("0")) {
			return new Static(false);
		} else {
			throw new IllegalArgumentException("Could not parse expression " + expression);
		}
	}
	
	/*
	 * Parses an argument List into an array of String
	 * e.g.: "NOT(x1),AND(x1,x2)" -> ["NOT(x1)", "AND(x1,x2)"]
	 */
	private static String[] parseArgs(String argList) {
		int depth = 0; // will track brackets
		int start = 0;
		ArrayList<String> args = new ArrayList<String>();
		
		for (int i = 0; i < argList.length(); i++) {
			switch (argList.charAt(i)) {
				case '(':
					depth++;
					break;
				case ')':
					depth--;
					break;
				case ',':
					// only add, if we are at the outermost expression
					if (depth == 0) {
						args.add(argList.substring(start, i));
						start = i+1;
					}
					break;
			}
		}
		if (depth != 0) {
			throw new IllegalArgumentException("Unequal amounts of '(' and ')': " + argList);
		}
		
		// add the remainder
		args.add(argList.substring(start));
		return args.toArray(new String[args.size()]);
	}
}
