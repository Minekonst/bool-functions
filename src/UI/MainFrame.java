package UI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import BoolFunction.FunctionTools;
import BoolFunction.Nodes.Node;

public class MainFrame extends JFrame {
	/*
	 * PrintStream used to intercept what is written to System.out
	 */
	private class Interceptor extends PrintStream
	{
	    StringBuilder stringBuilder = new StringBuilder();
		OutputStream outStream;
	    
		public Interceptor(OutputStream out)
	    {
	        super(out, true);
	        outStream = out;
	    }
	    
		@Override
	    public void print(String s)
	    {
	        try {
				outStream.write(s.getBytes());
			} catch (IOException e) {
				
			}
	        stringBuilder.append(s);
	    }
		
		
	    
		@Override
		public void println(String x) {
			try {
				outStream.write((x + "\n").getBytes());
			} catch (IOException e) {
				
			}
			stringBuilder.append(x + "\n");
		}

		/*
		 * Get the current dump, and clear it.
		 */
	    public String getContent() {
	    	String content = stringBuilder.toString();
	    	stringBuilder = new StringBuilder();
	    	return content;
	    }
	}
	
	private Interceptor interceptor;	
	
	public MainFrame() {
		super("bool-functions");
		setSize(800, 400);
		setLookAndFeel();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		initFrame();
		setVisible(true);
		
		PrintStream orig = System.out;
		interceptor = new Interceptor(orig);
		System.setOut(interceptor);
		
	}
	
	/*
	 * Initialize all UI-Components and link them together
	 */
	private void initFrame() {
		setLayout(new BorderLayout());
		
		JPanel left = new JPanel();
		left.setLayout(new BorderLayout());
		left.setBorder(new EmptyBorder(5, 5, 5, 5));
		JTextPane input = new JTextPane();
		left.add(input);
		

		
		
		JPanel right = new JPanel();
		right.setLayout(new BorderLayout());
		right.setBorder(new EmptyBorder(5, 5, 5, 5));
		JTextPane output = new JTextPane();
		output.setEditable(false);
		right.add(output);
		
		
		JSplitPane pane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, 
                left, right);
		pane.setResizeWeight(0.5);
		add(pane, BorderLayout.CENTER);
		
		JButton button = new JButton("Simplify");
		button.addActionListener((ActionEvent ev) ->{
			try {
				Node n = InputParser.parseInput(input.getText());
				Node n2 = FunctionTools.simplify(n, true);
				
				output.setText(interceptor.getContent() + "\nDone.");
			} catch (Exception e) {
				output.setText(e.getMessage());
			}
		});
		
		add(button, BorderLayout.SOUTH);
	}

	/*
	 * Switches to the native Look-and-Feel
	 */
	private void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}
}
